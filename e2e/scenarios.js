'use strict';

describe('XTA SUT App', function () {

  describe('Phone list view', function () {
    beforeEach(function () {
      browser.get('app/index.html#/phones');
    });

    it('should filter the phone list as a user types into the search box', function () {
      var phoneList = element.all(by.repeater('phone in phoneListCtrl.phones'));
      var query = element(by.model('query'));

      expect(phoneList.count()).toBe(20);

      query.sendKeys('nexus');
      expect(phoneList.count()).toBe(1);

      query.clear();
      query.sendKeys('motorola');
      expect(phoneList.count()).toBe(8);
    });
  });

  describe('Phone detail view', function () {
    beforeEach(function () {
      browser.get('app/index.html#/phones/nexus-s');
    });

    it('should display nexus-s page', function () {
      expect(element(by.binding('phone.name')).getText()).toBe('Nexus S');
    });

    it('should display the first phone image as the main phone image', function () {
      expect(element(by.id('main-mage')).getAttribute('src')).toMatch(/app-phonedata\/img\/phones\/nexus-s.0.jpg/);
    });
  });

  describe('slow calculator', function () {
    beforeEach(function () {
      browser.get('app/index.html#/calculator');
    });

    it('should add numbers', function () {
      element(by.model('calculatorCtrl.first')).sendKeys(4);
      element(by.model('calculatorCtrl.second')).sendKeys(5);

      element(by.id('gobutton')).click();

      expect(element(by.binding('latest')).getText()).toEqual('9');
    });

    describe('memory', function () {
      var first, second, goButton;

      beforeEach(function () {
        first = element(by.model('calculatorCtrl.first'));
        second = element(by.model('calculatorCtrl.second'));
        goButton = element(by.id('gobutton'));
      });

      it('should start out with an empty memory', function () {
        var memory = element.all(by.repeater('result in calculatorCtrl.memory'));
        expect(memory.count()).toEqual(0);
      });

      it('should fill the memory with past results', function () {
        first.sendKeys(1);
        second.sendKeys(1);
        goButton.click();

        first.sendKeys(10);
        second.sendKeys(20);
        goButton.click();

        var memory = element.all(by.repeater('result in calculatorCtrl.memory').column('result.value'));
        memory.then(function (arr) {
          expect(arr.length).toEqual(2);
          expect(arr[0].getText()).toEqual('30'); // 10 + 20 = 30
          expect(arr[1].getText()).toEqual('2'); // 1 + 1 = 2
        });
      });
    });
  });
});
