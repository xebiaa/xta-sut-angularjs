'use strict';

angular.module('xtaSut.calculator', ['ngRoute'])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/calculator', {
      controller: 'CalculatorCtrl',
      controllerAs: 'calculatorCtrl',
      templateUrl: 'calculator/calculator.html'
    });
  }])

  .controller('CalculatorCtrl', function ($timeout) {
    var self = this;

    self.memory = [];
    self.latest = 0;
    self.operators = {
      ADDITION: '+',
      SUBTRACTION: '-',
      MULTIPLICATION: '*',
      DIVISION: '/',
      MODULO: '%'
    };
    self.operator = self.operators.ADDITION;

    self.doAddition = function () {
      var times = 5;
      self.latest = '. ';
      $timeout(function tickslowly() {
        if (times == 0) {
          var latestResult;
          var first = parseInt(self.first);
          var second = parseInt(self.second);
          switch (self.operator) {
            case '+':
              latestResult = first + second;
              break;
            case '-':
              latestResult = first - second;
              break;
            case '*':
              latestResult = first * second;
              break;
            case '/':
              latestResult = first / second;
              break;
            case '%':
              latestResult = first % second;
              break;
          }
          self.memory.unshift({
            timestamp: new Date(),
            first: self.first,
            operator: self.operator,
            second: self.second,
            value: latestResult
          });
          self.first = self.second = '';
          self.latest = latestResult;
        } else {
          self.latest += '. ';
          times--;
          $timeout(tickslowly, 300);
        }
      }, 300)
    };
  });

